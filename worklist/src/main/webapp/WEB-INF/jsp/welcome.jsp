<%@ include file="common/header.jspf"%>
<%@ include file="common/navigation.jspf"%>

<div class="container">
	<h2>Welcome, ${name} </h2>
	
	<img alt="worklist-image" width="300"  height="500" align="right" 
		src="https://i.gifer.com/EC1d.gif">
	<br/>
	
		<ul style="list-style-type:circle; font-size:18px; font-style: oblique; margin-top: 20; margin-bottom: 35">
		    <li>Checklists remind us of the minimum necessary steps and make them explicit. 
			They not only offer the possibility of verification but also 
			instill a kind of discipline of higher performance</li>
			<li>Good checklists, on the other hand are precise. 
			They are efficient, to the point, and easy to use even in the most difficult situations. 
			They do not try to spell out everything--a checklist cannot fly a plane. 
			Instead, they provide reminders of only the most critical and important steps--the ones that even the highly skilled professional using them could miss. 
			Good checklists are, above all, practical.</li>
		</ul>
	
	
	<div style="margin-top: 15">
		<a type="button" class="btn btn-primary btn-lg" 
	 	href="/list-todos">Click here</a> to manage your task's.
	</div>
	 
</div>
<%@ include file="common/footer.jspf"%>