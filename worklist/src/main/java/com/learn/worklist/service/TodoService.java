package com.learn.worklist.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.springframework.stereotype.Service;

import com.learn.worklist.model.Todo;

@Service
public class TodoService {
    
	private static List<Todo> todos = new ArrayList<Todo>();
    private static int todoCount1 = 1003;
    private static int todoCount2 = 2003;

    static {
        todos.add(new Todo(1001, "user1", "Learn Spring MVC", new Date(), false));
        todos.add(new Todo(1002, "user1", "Learn Java", new Date(), false));
        todos.add(new Todo(1003, "user1", "Learn Hibernate", new Date(), false));
    }
    
    static {
        todos.add(new Todo(2001, "user2", "Timesheets", new Date(), false));
        todos.add(new Todo(2002, "user2", "Do workout", new Date(), false));
        todos.add(new Todo(2003, "user2", "Learn JDBC", new Date(), false));
    }
    

    public List<Todo> retrieveTodos(String user) {
        List<Todo> filteredTodos = new ArrayList<Todo>();
        for (Todo todo : todos) {
            if (todo.getUser().equalsIgnoreCase(user)) {
                filteredTodos.add(todo);
            }
        }
        return filteredTodos;
    }
    
    public Todo retrieveTodo(int id) {
        for (Todo todo : todos) {
            if (todo.getId()==id) {
                return todo;
            }
        }
        return null;
    }

    public void updateTodo(Todo todo){
    		todos.remove(todo);
    		todos.add(todo);
    }

    public void addTodo(String name, String desc, Date targetDate, boolean isDone) {
    	if(name.equalsIgnoreCase("user1")) {
    		todos.add(new Todo(++todoCount1, name, desc, targetDate, isDone));
    	}else {
    		todos.add(new Todo(++todoCount2, name, desc, targetDate, isDone));
    	}
        
    }

    public void deleteTodo(int id) {
        Iterator<Todo> iterator = todos.iterator();
        while (iterator.hasNext()) {
            Todo todo = iterator.next();
            if (todo.getId() == id) {
                iterator.remove();
            }
        }
    }
}